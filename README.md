[![pipeline status](https://gitlab.com/umiami/george/csc220/lab01/badges/master/pipeline.svg)](https://gitlab.com/umiami/george/csc220/lab01/-/pipelines?ref=master)
[![compile](https://gitlab.com/umiami/george/csc220/lab01/builds/artifacts/master/raw/.results/compile.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/lab01/-/jobs/artifacts/master/file/.results/compile.log?job=evaluate)
[![checkstyle](https://gitlab.com/umiami/george/csc220/lab01/builds/artifacts/master/raw/.results/checkstyle.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/lab01/-/jobs/artifacts/master/file/.results/checkstyle.log?job=evaluate)
[![test](https://gitlab.com/umiami/george/csc220/lab01/builds/artifacts/master/raw/.results/test.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/lab01/-/jobs/artifacts/master/file/.results/test.log?job=evaluate)

# Lab01: Introduction

> This project is designed to introduce programing environment to students. It prints out `Hello world!` string and simulates a coin flip with an uneven probability. Additionally, it analyzes arrays of integers searching for specific value pairs.
